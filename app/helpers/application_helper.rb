module ApplicationHelper
  # ZONA PUBLICA
  def set_active_home
    params[:controller] == 'home' ? 'class=active' : ''
  end
  def set_active_products
    params[:controller] == 'products' ? 'active' : ''
  end
  def set_active_contact
    params[:controller] == 'contact' ? 'class=active' : ''
  end
  def set_active_blog
    params[:controller] == 'blog' ? 'class=active' : ''
  end
  def set_active_how_do
    params[:controller] == 'how_do' ? 'class=active' : ''
  end
  # ZONA DE ADMINISTRACION
  ########## usuarios ##########
  def set_active_admin_home_index
    params[:action] == 'index' && params[:controller] == 'admin/home' ? 'active' : ''
  end
  def set_active_admin_users
    params[:controller] == 'admin/users' ? 'active' : ''
  end
  def set_active_admin_users_index
    params[:action] == 'index' && params[:controller] == 'admin/users' ? 'active' : ''
  end
  def set_active_admin_users_edit
    params[:action] == 'edit' && params[:controller] == 'admin/users' ? 'active' : ''
  end
  def set_active_admin_users_new
    params[:action] == 'new' && params[:controller] == 'admin/users' ? 'active' : ''
  end

  ########## productos ##########
  def set_active_admin_products
    params[:controller] == 'admin/products' || params[:controller] == 'admin/product_categories' ? 'active' : ''
  end
  def set_active_admin_products_index
    params[:action] == 'index' && params[:controller] == 'admin/products' ? 'active' : ''
  end
  def set_active_admin_products_new
    params[:action] == 'new' && params[:controller] == 'admin/products' ? 'active' : ''
  end
  ########## categorias ##########
  def set_active_admin_categories
    params[:controller] == 'admin/categories' || params[:controller] == 'admin/categories' ? 'active' : ''
  end
  def set_active_admin_categories_index
    params[:action] == 'index' && params[:controller] == 'admin/categories' ? 'active' : ''
  end
  ############ gastos de envio ###############
  def set_active_admin_delivery_prices
    params[:controller] == 'admin/delivery_prices' ? 'active' : ''
  end
  def set_active_admin_delivery_prices_index
    params[:action] == 'index' && params[:controller] == 'admin/delivery_prices' ? 'active' : ''
  end
  def set_active_admin_delivery_prices_new
    params[:action] == 'new' && params[:controller] == 'admin/delivery_prices' ? 'active' : ''
  end
  ########## pedidos ##########
  def set_active_admin_orders
    params[:controller] == 'admin/orders' || params[:controller] == 'admin/orders' ? 'active' : ''
  end
  def set_active_admin_orders_index
    params[:action] == 'index' && params[:controller] == 'admin/orders' ? 'active' : ''
  end
  ############## blog ################
  def set_active_admin_posts
    params[:controller] == 'admin/posts' || params[:controller] == 'admin/post' ? 'active' : ''
  end
  def set_active_admin_posts_index
    params[:action] == 'index' && params[:controller] == 'admin/posts' ? 'active' : ''
  end
  def set_active_admin_posts_new
    params[:action] == 'new' && params[:controller] == 'admin/posts' ? 'active' : ''
  end
  ############## comentarios del blog #######
  def set_active_admin_comments
    params[:controller] == 'admin/comments' ? 'active' : ''
  end
  def set_active_admin_comments_index
    params[:action] == 'index' && params[:controller] == 'admin/comments' ? 'active' : ''
  end
  ########## categorias para el blog ##########
  def set_active_admin_post_categories
    params[:controller] == 'admin/post_categories' || params[:controller] == 'admin/post_categories' ? 'active' : ''
  end
  def set_active_admin_post_categories_index
    params[:action] == 'index' && params[:controller] == 'admin/post_categories' ? 'active' : ''
  end

  # ZONA DE ADMINISTRACION PARA EL CLIENTE
  ########## usuario ##########
  def set_active_customer_user
    params[:controller] == 'users/registrations' ? 'active' : ''
  end
  def set_active_customer_user_update
    params[:action] == 'edit' && params[:controller] == 'users/registrations' ? 'active' : ''
  end

end
