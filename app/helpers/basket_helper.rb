module BasketHelper
  def total_products #precio del carrito actual por productos
    total=0
    unique_products_basket.each do |element|
      product=Product.find(element)
      total=total+(product.price*count_num_products_in_basket(product).to_f)
    end
    total
  end
  # productos unicos en la cesta
  def unique_products_basket
    session[:basket].uniq #elimina duplicados
  end

  def delivery_cart_price #gastos de envio del carrito actual por peso
    total=0
    session[:basket].uniq.each do |product|
      p=Product.find(product)
      count=count_num_products_in_basket(p)
      total+=p.gr*count
    end
    DeliveryPrice.where("gr >= #{total}").first.price
  end

  # cuenta el numero de productos de un producto en la cesta
  def count_num_products_in_basket(product)
    session[:basket].count(product.id.to_s)
  end
end
