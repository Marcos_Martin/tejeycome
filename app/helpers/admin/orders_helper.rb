module Admin::OrdersHelper
  def payment_options
    [[t('order.pay_method1')],[t('order.pay_method2')]]
  end
  def order_state
    [[t('order.state1')],[t('order.state2')],[t('order.state3')]]
  end
end
