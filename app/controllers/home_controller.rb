class HomeController < ApplicationController
  def index
    @carousel_products=Product.random4
    @carousel_posts=Post.random4
    @last_4_products=Product.last(4).reverse
    @last_4_posts=Post.last(4).reverse
    @web_title='TejeyCome.es - Home'
  end
  def legal
    @web_title='TejeyCome.es - Aviso Legal'
  end

end
