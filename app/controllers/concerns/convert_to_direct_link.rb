module ConvertToDirectLink
  extend ActiveSupport::Concern
  include DirectLink
  def get_direct_link(url)
    evalue=url
    if url_is_google_drive?(url) # si la url es de google drive se transforma a link directo
      evalue=get_google_direct_link(url)
    end
    evalue
  end
  def check_to_direct_link # llamar a esta accion por Post para que devuelva en el elemento con id=direct_link la url transformada
    link=get_direct_link(params[:search][:url])
    respond_to do |format|
      format.js {render(inline: "document.getElementById(\"direct_link\").innerHTML = \"#{link}\";")}
    end
  end
end