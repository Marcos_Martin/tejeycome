class CommentsController < ApplicationController
  def create
    # procesamiento captcha negativos
    # para poner mas trampas solo crea @bot_field2,3,4... y separalos con un || en are_you_a_bot?
    @bot_field1=params[:name]
    if are_you_a_bot?
      reset_session
      render text: '', status: '404'
    else
      @comment = Comment.new(comment_params)
      #de momento solo tendre comentarios para Blog
      @comment.commentable_id = Post.find_by_slug(params[:blog_id]).id
      @comment.commentable_type = Post::name
      #@message = Message.new(name: @comment.user,
      #                      surname: '_',
      #                      email: @comment.email,
      #                      subject: @url+'blogs/'+@comment.commentable.slug,
      #                      body: @comment.body,
      #                      phone: '_')
      respond_to do |format|
        if @comment.valid? && @comment.save
          # Por si luego quiero ser notificado por cada comentario
          #CommentNotifier.contact_me(@message).deliver
          session[:comment_body]=nil
          session[:comment_name]=nil
          session[:comment_email]=nil
          session[:comment_post_id]=nil
          format.html {
            redirect_to blog_path(Post.find_by_slug(params[:blog_id])), notice: t('comment.create')
          }
        else
          format.html {
            session[:comment_post_id]=@comment.commentable_id
            session[:comment_body]=@comment.body
            user_signed_in? ? session[:comment_user]=current_user.name : session[:comment_user]=@comment.user
            user_signed_in? ? session[:comment_email]=current_user.email : session[:comment_email]=@comment.email
            redirect_to blog_path(Post.find_by_slug(params[:blog_id])), alert: t('comment.create_fail')
          }
        end
      end
    end
  end

  private
  def are_you_a_bot?
    @bot_field1.present? ? true : false
  end
  def comment_params
    params.require(:comment).permit(:body, :user, :email, :replied_of_id, :humanizer_answer, :humanizer_question_id)
  end
end

