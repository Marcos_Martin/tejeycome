class Users::RegistrationsController < Devise::RegistrationsController
before_filter :configure_sign_up_params, only: [:create]
before_filter :configure_account_update_params, only: [:update]
layout 'admin/admin', only: [:edit]

  def new
    @web_title='TejeyCome.es - Nuevo Usuario'
    super
  end

def create
    super
end

  def edit
    @web_title='TejeyCome.es - Editar usuario'
    super
  end

  def update
    resource.update_without_password(account_update_params)
    redirect_to :back, notice: t('admin_zone.users.update_notify')
  end

  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
   def configure_sign_up_params
     devise_parameter_sanitizer.for(:sign_up) << :name << :surname1 << :surname2 << :address << :phone << :humanizer_answer << :humanizer_question_id
   end

  # If you have extra params to permit, append them to the sanitizer.
   def configure_account_update_params
     devise_parameter_sanitizer.for(:account_update) << :name << :surname1 << :surname2 << :address << :phone << :humanizer_answer << :humanizer_question_id
   end

  #The path used after sign up.
  def after_sign_up_path_for(resource)
    super(resource)
  end

# The path used after sign up for inactive accounts.
# def after_inactive_sign_up_path_for(resource)
#   super(resource)
# end
end
