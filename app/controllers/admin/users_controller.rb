class Admin::UsersController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  layout 'admin/admin'
  before_action :set_user, only: [:edit, :update, :destroy]
  #before_action :user_params, only: [:update]

  def index
    @users=User.paginate(page: params[:page], per_page: User::PAGE_INDEX).order(created_at: :desc)
    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def update

    #se grabara aunque venga sin contraseña
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    end

    respond_to do |format|
      if @user.update_attributes(params.require(:user).permit(:name, :password,:password_confirmation,:surname1,
                                                              :surname2 , :address , :phone ,:active,:role_id))
        format.html { redirect_to admin_users_path, notice: t('admin_zone.users.update_notify') }
      else
        format.html { render action: "edit", alert: t('admin_zone.users.update_alert') }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to  admin_users_path, notice: t('admin_zone.users.destroy_notify')  }
    end
  end

  def search
    param=params[:search][:text]
    search= User.where("name LIKE '%#{param}%' OR
                       email LIKE '%#{param}%' OR
                      phone LIKE '%#{param}%'")

    @users = search.order(created_at: :desc).paginate(page: params[:page], per_page: User::PAGE_INDEX)
    render "index"
  end

  def sort
    @users = User.paginate(page: params[:page], per_page: User::PAGE_INDEX)
    if params[:by]=='name_desc'
      @users = @users.order('name DESC')
    elsif params[:by]=='name_asc'
      @users =@users.order('name ASC')
    elsif params[:by]=='surname1_desc'
      @users = @users.order('surname1 DESC')
    elsif params[:by]=='surname1_desc'
      @users = @users.order('surname1 ASC')
    elsif params[:by]=='disable'
      @users = @users.disable.order('name ASC')
    elsif params[:by]=='admin_users'
      @users = @users.admins.order('name ASC')
    end

    render "index"
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

end
