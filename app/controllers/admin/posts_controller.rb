class Admin::PostsController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  layout 'admin/admin'
  def index
    @posts=Post.paginate(page: params[:page], per_page: Post::PAGE_INDEX).order('created_at DESC')
    @post_categories_total= Post.all.count
    respond_to do |format|
      format.html
      format.json { render json: @post }
    end
  end
  def show
    @post=Post.find(params[:id])

  end
  def new
    @post=Post.new
    #@load_javascript="run_rich_text_editor('es-ES');"
    respond_to do |format|
      format.html
      format.json { render json: @post }
    end
  end

  def edit
    @post = Post.find(params[:id])
    #@load_javascript="run_rich_text_editor('es-ES');"
  end

  def update
    @post = Post.find(params[:id])
    respond_to do |format|
      if @post.update_attributes(post_params)
        format.html { redirect_to admin_post_path, notice: t('admin_zone.posts.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @post = Post.new(post_params)
    respond_to do |format|
      if @post.save
        format.html { redirect_to  admin_posts_path, notice: t('admin_zone.posts.create') }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    flash[:alert]= t('admin_zone.posts.destroy')
    respond_to do |format|
      format.html { redirect_to  admin_posts_path  }
      format.json { head :no_content }
    end
  end
  def search
    param=params[:search][:text]
    search= Post.where("title LIKE '%#{param}%'")
    @posts = search.paginate(page: params[:page], per_page: Post::PAGE_INDEX).order('created_at DESC')
    @subtitle=param
    render "index"
  end
  def sort
    case params[:by]
      when 'draf_posts'
        @posts=Post.drafts.paginate(page: params[:page], per_page: Post::PAGE_INDEX).order('created_at DESC')
      else
        @products = Post.paginate(page: params[:page], per_page: Post::PAGE_INDEX).order('created_at DESC')
    end
    @subtitle=t('admin_zone.posts.draft')
    render "index"
  end
  def category
    param=params[:category]
    search= Post.where(post_category_id: PostCategory.find_by(name: param).id)
    @posts = search.paginate(page: params[:page], per_page: Post::PAGE_INDEX).order('created_at DESC')
    @subtitle=param
    render "index"
  end

  private
  def post_params
    params.require(:post).permit(:body, :title,:draft,:media,:post_category_id)
  end
end
