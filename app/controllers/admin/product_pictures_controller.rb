class Admin::ProductPicturesController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  def create
    @product_picture = ProductPicture.new(product_picture_params)
    @product_picture.product_id=Product.find(params[:product_id]).id

    respond_to do |format|
      if @product_picture.save
        format.html { redirect_to edit_admin_product_path(@product_picture.product),
                                  notice: t('admin_zone.navbar.product_pictures.create_ok') }
      else
        format.html { redirect_to edit_admin_product_path(@product_picture.product),
                                  alarm: t('admin_zone.navbar.product_pictures.create_bad') }
      end
    end
  end

  def destroy
    @product_picture = ProductPicture.find(params[:id])
    @product_picture.destroy


    respond_to do |format|
      format.html { redirect_to edit_admin_product_path(@product_picture.product),
                                notice: t('admin_zone.navbar.product_pictures.destroy_ok') }
    end
  end

  private
  def product_picture_params
    params.require(:product_picture).permit(:media)
  end
end
