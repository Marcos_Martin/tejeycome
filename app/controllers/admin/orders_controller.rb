class Admin::OrdersController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  layout 'admin/admin'
  before_action :set_order, only: [:update, :destroy]

  def index
    if current_user.is_customer?
      @orders = Order.from_current_customer(current_user).paginate(page: params[:page], per_page: Order::PAGE_INDEX).order('created_at DESC')
      render :index_customer

    elsif current_user.is_admin?
      @orders = Order.paginate(page: params[:page], per_page: Order::PAGE_INDEX).order('created_at DESC')
    end

  end

  def update
    respond_to do |format|
      if @order.update(order_params)
        # mando email al cliente si se pidio en el formulario
        OrderNotifier.change_order(@order, @web_title).deliver_now if params[:send_mail_customer]
        format.html { redirect_to :back, notice: t('admin_zone.orders.edit_notify', ref: @order.id) }
      else
        format.html { render :index,  t('admin_zone.orders.edit_alert',ref: @order.id)  }
      end
    end
  end

  def search
    param=params[:search][:text]
    # busqueda por email del cliente o por ref
    if current_user.is_customer?
      search= Order.where(user_id: User.where(email: param)).union(Order.where(id: param)).where(user_id: current_user.id)
      @orders = search.paginate(page: params[:page], per_page: Order::PAGE_INDEX)
      render "index_customer"
     elsif current_user.is_admin?
      search= Order.where(user_id: User.where(email: param)).union(Order.where(id: param))
      @orders = search.paginate(page: params[:page], per_page: Order::PAGE_INDEX)
      render "index"
    end
  end

  def sort
    case params[:by]
      when 'outstanding_payment'
        @subtitle=t('admin_zone.home.outstanding_payment')
        @orders=Order.outstanding_payment.paginate(page: params[:page], per_page: Order::PAGE_INDEX).order('created_at DESC')
      when 'pending_shipping'
        @subtitle=t('admin_zone.home.pending_shipping')
        @orders=Order.pending_shipping.paginate(page: params[:page], per_page: Order::PAGE_INDEX).order('created_at DESC')
      when 'orders_this_month'
        @subtitle=t('admin_zone.home.orders_this_month')
        @orders=Order.this_month.paginate(page: params[:page], per_page: Order::PAGE_INDEX).order('created_at DESC')
      else
        @orders = Order.paginate(page: params[:page], per_page: Order::PAGE_INDEX).order('created_at DESC')
    end
    render "index"
  end

  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to  admin_products_path , notice: t('admin_zone.orders.destroy_notify') }
    end
  end

  private
  def set_order
    @order = Order.find(params[:id])
  end

  def order_params
    params.require(:order).permit(:address, :pay_method,:comment,:state, :cod_delivery, :price)
  end
end
