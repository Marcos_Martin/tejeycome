class Admin::DeliveryPricesController < ApplicationController
  layout "admin/admin"
  def index
    @delivery_prices=DeliveryPrice.all
    respond_to do |format|
      format.html
      format.json { render json: @delivery_prices }
    end
  end

  def new
    @delivery_price=DeliveryPrice.new
    #@load_javascript="run_rich_text_editor('es-ES');"
    respond_to do |format|
      format.html
      format.json { render json: @delivery_price }
    end
  end

  def edit
    @delivery_price = DeliveryPrice.find(params[:id])
    #@load_javascript="run_rich_text_editor('es-ES');"
  end

  def update
    @delivery_price = DeliveryPrice.find(params[:id])

    respond_to do |format|
      if @delivery_price.update_attributes(delivery_prices_params)
        format.html { redirect_to admin_delivery_prices_path, notice: t('delivery_prices.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @delivery_price.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @delivery_price = DeliveryPrice.new(delivery_prices_params)
    respond_to do |format|
      if @delivery_price.save
        format.html { redirect_to  admin_delivery_prices_path, notice: t('delivery_prices.new') }
        format.json { render json: @delivery_price, status: :created, location: @delivery_price }
      else
        format.html { render action: "new" }
        format.json { render json: @delivery_price.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @delivery_price = DeliveryPrice.find(params[:id])
    @delivery_price.destroy
    flash[:alert]= t('delivery_prices.destroy')
    respond_to do |format|
      format.html { redirect_to  admin_delivery_prices_path  }
      format.json { head :no_content }
    end
  end

  def delivery_prices_params
    params.require(:delivery_price).permit(:gr, :price)
  end
end
