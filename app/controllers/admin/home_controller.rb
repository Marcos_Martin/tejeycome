class Admin::HomeController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  authorize_resource class: false
  layout 'admin/admin'
  include ConvertToDirectLink

  def index
    if current_user.is_customer?
      @last_order=@orders = Order.from_current_customer(current_user).last
      render :index_customer
    elsif current_user.is_admin?
      @area_chart_orders_this_month=Order.this_month.group_by_day(:created_at, format: "%d/%B/%Y").maximum(:price)
      @pie_products_by_main_categories=calc_pie_products_by_main_categories
      @column_chart_by_users=User.this_year.group_by_month(:created_at,format: "%d/%b/%Y").count
      render :index
    end
  end

  private
  def calc_pie_products_by_main_categories
    result=Array.new
    num_products=0
    Category.mains.each do |category|
      num_products+=category.products.count
      category.subcategories.each do |subcategory|
        num_products+=subcategory.products.count
      end
      result << [category.name,num_products]
      num_products=0
    end
    result
  end
end
