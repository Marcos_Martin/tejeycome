class Admin::ProductsController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  layout 'admin/admin'
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.paginate(page: params[:page], per_page: Product::PAGE_INDEX).order('created_at DESC')
  end

  def new
    @product = Product.new
  end

  def edit
    @product_picture=ProductPicture.new
  end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to  edit_admin_product_path (@product), notice: t('admin_zone.products.new_notify') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to edit_admin_product_path (@product), notice: t('admin_zone.products.edit_notify') }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    respond_to do |format|
      format.html { redirect_to  admin_products_path , notice: t('admin_zone.products.destroy_notify') }
    end
  end

  def search
    param=params[:search][:text]
    search= Product.where("name LIKE '%#{param}%' OR
                        id LIKE '%#{param}%' ")
    @products = search.paginate(page: params[:page], per_page: Product::PAGE_INDEX).order('created_at DESC')
    @subtitle=param
    render "index"
  end

  def sort
    case params[:by]
      when 'active_products'
        @subtitle=t('admin_zone.home.active_products')
        @products=Product.actives.paginate(page: params[:page], per_page: Product::PAGE_INDEX).order('created_at DESC')
      when 'deactivated_products'
        @subtitle=t('admin_zone.home.deactivated_products')
        @products=Product.deactivates.paginate(page: params[:page], per_page: Product::PAGE_INDEX).order('created_at DESC')
      else
        @products = Product.paginate(page: params[:page], per_page: Product::PAGE_INDEX).order('created_at DESC')
    end
    render "index"
  end
  def category
    param=params[:category]
    if Category.find_by(name: param).have_subcategories?
      search=(Product.where(category_id: Category.find_by(name: param).subcategories).
          union(Product.where(category_id: Category.find_by(name: param))))
    else
      search= Product.where(category_id: Category.find_by(name: param).id)
    end
    @products = search.paginate(page: params[:page], per_page: Product::PAGE_INDEX).order('created_at DESC')
    @subtitle=params[:category]
    render "index"
  end


  private

  def set_product
    @product = Product.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def product_params
    params.require(:product).permit(:name, :description,:price,:media, :active, :category_id,:gr)
  end
end
