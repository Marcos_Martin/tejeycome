class Admin::CommentsController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  layout 'admin/admin'

  def index
    @comments = Comment.all
    @comments=Comment.paginate(page: params[:page], per_page: Comment::PAGE_INDEX).order('created_at DESC')
    @comments_total= Comment.all.count

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comments }
    end
  end


  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end


  def new
    @comment = Comment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end


  def edit
    @comment = Comment.find(params[:id])
  end


  def create
    @comment = Comment.new(comment_params)

    respond_to do |format|
      if @comment.save
        format.html { redirect_to admin_comments_path, notice: t('admin_zone.comment.create') }
        format.json { render json: @comment, status: :created, location: @comment }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @comment = Comment.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(comment_params)
        format.html { redirect_to admin_comments_path, notice: t('admin_zone.comment.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  def search
    param=params[:search][:text]
    search= Comment.where("body LIKE '%#{param}%' OR
                           email LIKE '%#{param}%' OR
                           user LIKE '%#{param}%'")
    @comments = search.paginate(page: params[:page], per_page: Comment::PAGE_INDEX)
    @comments_total= Comment.all.count
    render "index"
  end

  def destroy
    @comments = Comment.find(params[:id])
    @comments.destroy
    respond_to do |format|
      format.html { redirect_to  admin_comments_path , notice: t('admin_zone.comment.destroy_notify') }
    end
  end

  def comment_params
    params.require(:comment).permit(:body, :user,:email,:replied_of_id)
  end
end

