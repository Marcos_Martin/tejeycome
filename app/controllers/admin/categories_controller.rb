class Admin::CategoriesController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  layout 'admin/admin'
  before_action :set_category, only: [:update, :destroy]


  def index
    #listare todas sin paginar (no seran muchas)
    @categories = Category.where(parent_id: nil)
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to  admin_categories_path,
                                                            notice: t('admin_zone.categories.new_notify') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to admin_categories_path,
                                  notice: t('admin_zone.categories.edit_notify') }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @category.destroy
        format.html { redirect_to  admin_categories_path ,
                                   notice: t('admin_zone.categories.destroy_notify') }
      else
        format.html {redirect_to  admin_categories_path ,
                                 alert: t('admin_zone.categories.destroy_alarm')  }
      end
    end
  end

  private
  def set_category
    @category = Category.find(params[:id])
  end


  def category_params
    params.require(:category).permit(:name, :parent_id)
  end
end
