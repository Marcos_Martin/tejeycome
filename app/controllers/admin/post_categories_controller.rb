class Admin::PostCategoriesController < ApplicationController
  before_filter :authenticate_user! # es necesario estar autenticado para continuar
  load_and_authorize_resource # carga los permisos para el modelo por defecto de este controlador
  layout 'admin/admin'
  before_action :set_category, only: [:update, :destroy]


  def index
    #listare todas sin paginar (no seran muchas)
    @post_categories = PostCategory.all
    @post_category = PostCategory.new
  end

  def create
    @post_category = PostCategory.new(post_category_params)

    respond_to do |format|
      if @post_category.save
        format.html { redirect_to  admin_post_categories_path,
                                                            notice: t('admin_zone.categories.new_notify') }
      else
        format.html { render :index }
      end
    end
  end

  def update
    respond_to do |format|
      if @post_category.update(post_category_params)
        format.html { redirect_to admin_post_categories_path,
                                  notice: t('admin_zone.categories.edit_notify') }
      else
        format.html { render :index }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @post_category.destroy
        format.html { redirect_to  admin_post_categories_path ,
                                   notice: t('admin_zone.categories.destroy_notify') }
      else
        format.html {redirect_to  admin_post_categories_path ,
                                 alert: t('admin_zone.categories.destroy_alarm')  }
      end
    end
  end

  private
  def set_category
    @post_category = PostCategory.find(params[:id])
  end


  def post_category_params
    params.require(:post_category).permit(:name)
  end
end
