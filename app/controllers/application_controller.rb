class ApplicationController < ActionController::Base
  include CanCan::ControllerAdditions
  protect_from_forgery with: :exception
  before_action :web_default #variables que se inician por defecto
  before_filter :init_basket #inicializamos la cesta si es necesario

  rescue_from CanCan::AccessDenied do |exception| #seguridad  modelos y controladores
    flash[:alert] = t('common.no_permited')
    session[:user_return_to] = request.url
    redirect_to root_path
  end


  def after_sign_in_path_for(user) #despues de identificarnos, nos vamos a...
    # si el usuario esta inactivo, no se le permite identificarse
    # si hay productos en el carrito, nos vamos a crear un pedido tras identificarnos
    # esto es por si un usuario acaba de registrarse para poder hacer un pedido

    unless user.active
      sign_out user
      flash[:notice]=nil
      flash[:alert]= t('devise.failure.inactive')
      root_path
    else
      if session[:basket].count > 0
        new_order_path
      else
        admin_home_path
      end
    end
  end

  # cesta de la compra
  # inicializacion de la cesta
  def init_basket
    session[:basket] ||= Array.new #si esta vacio le pone el array de productos vacio
    @basket ||= Array.new
    @basket_total=session[:basket].count
  end

  # cesta de la compra


  private
  def web_default
    I18n.locale = params[:locale] || I18n.default_locale # configuracion del idioma
    params[:controller] == 'devise/sessions' ?
        @web_title="#{t('common.name_website')} - #{t('devise.links.sign_in')}" :
        @web_title= t('common.name_website')
    @description= t('home.description')
    @url= request.original_url
    @image= request.base_url+'/assets/logo.png'
    @keywords= t('common.keywords')
    @load_javascript=""
  end

end
