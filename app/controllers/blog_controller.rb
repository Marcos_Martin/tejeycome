class BlogController < ApplicationController
  respond_to :html
  layout "do_pdf", only: [:do_pdf]
  def index
    @posts= Post.actives.paginate(page: params[:page], per_page: Post::PAGE_INDEX).order('created_at DESC')
    @web_title='TejeyCome.es - Come'
    @description= t('blog.description_seo')
    respond_with(@posts)
  end

  def show
    @post = Post.find(params[:id])
    if @post.draft
      redirect_to(blog_index_path, alert: t('blog.post_draft'))
    else
      @post.id == session[:comment_post_id]? @comment= Comment.new(user: session[:comment_user], email: session[:comment_email], body: session[:comment_body]) : @comment= Comment.new
      @post.id == session[:comment_post_id]? @comment.valid? : @comment
      @comment.change_humanizer_question # cambiar la pregunta del captcha
      @comments= @post.comments.order('created_at DESC')
      @web_title="TejeyCome.es - Come [#{@post.title}]"
      @url=request.original_url
      @image=@post.media
      @description="(#{@post.post_category.name}) #{@post.title}: "+Sanitize.fragment(@post.body ).split[0...80].join(' ')
      @keywords= t('common.keywords')+", #{@post.title}"
      respond_with(@post,@comments,@comment)
    end
  end

  def category
    param=params[:category]
    @web_title="TejeyCome.es - Come (#{param})"
    search= Post.actives.where(post_category_id: PostCategory.find_by(name: param).id).order('created_at DESC')
    @posts = search.paginate(page: params[:page], per_page: Post::PAGE_INDEX)
    render "index"
  end

  def do_pdf
    @post = Post.find(params[:post])
    @url= request.base_url+blog_path(@post)
    @qrcode = RQRCode::QRCode.new(@url).as_svg(offset: 0, color: '000',
                                               shape_rendering: 'crispEdges',
                                               module_size: 2)
    @contact_path= request.base_url+contact_index_path
    respond_to do |format|
      format.pdf do
        render pdf: "[#{@web_title}] "+@post.title, encoding: 'utf8'
      end
    end
  end
end
