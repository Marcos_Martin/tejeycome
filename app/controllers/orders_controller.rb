class OrdersController < ApplicationController
  include BasketHelper
  protect_from_forgery except: :show #hace que al volver de paypal no se rompa la sesion de Devise
  respond_to :html

  def show # en esta accion no se vigila el token de autenticacion por la vuelta con PayPal, es importante vigilar de cerca la seguridad
    @web_title="TejeyCome.es - Pedido [#{params[:id]}]"
    @order = Order.find(params[:id])
    #vigilo que el usuario que viene de un pedido de PayPal sea el que lo creo o administrador
    if @order.user == current_user  || @order.user.is_admin?
      if (params[:payment_status] == 'Pending' || params[:payment_status] == 'Completed') && params[:custom] == @order.token_paypal
      # observo si se pago correctamente por PayPal y la seguridad
        # Pending= pendiente de aceptar el pago por el vendedor (pago correcto), Completed= pago completamente realizado
        @order.update_attribute(:state, t('order.state2'))
        @order.update_attribute(:token_paypal, nil) # libero espacio de la base de datos inutil
        # Mando un mail al cliente avisandole de su pedido por PayPal
        OrderNotifier.paypal(@order, @web_title).deliver_now
        # Mando email al administrador para que sepa de un pedido nuevo
        OrderNotifier.admin_notify_order(@order, @web_title).deliver_now
        flash[:notice] = t('order.new', ref: @order.id)
      else # por si existe algun problema  con el pago por PayPal
        flash[:alert] = t('order.new_alert', ref: @order.id) if @order.state == t('order.state1')
      end
      flash[:notice] = t('order.new', ref: @order.id)
      respond_with(@order)
    else # si no eres el usuario que hizo el pedido, redireccion a root_path
      redirect_to root_path, alert: t('common.no_permited')
    end
  end

  def new
    @web_title='TejeyCome.es - Nuevo Pedido'
    if user_signed_in?
      if total_products > 0
        @load_javascript += 'run_wysihtml5();'
        @credit_card = CreditCard.new
        @show_credit_card_form=false
        @order = Order.new(user_id: current_user.id,
                           address: current_user.address)
        respond_with(@order, @show_credit_card_form, @credit_card)
      else
        redirect_to :back, alert: t('product.nothing')
      end
    else
      redirect_to new_user_registration_path, alert: t('order.need_register')
    end
  end

  def create
    # doy permisos y agrego parametros al pedido
    @order = Order.new(order_params)
    # agrego ususario
    @order.user_id=current_user.id
    #pongo los gastos de envio
    @order.delivery_price=view_context.delivery_cart_price
    # agrego precio total del pedido
    @order.price=total_products+@order.delivery_price
    #guardo el pedido
    @order.save
    # creo un carrito para este pedido lleno de productos
    unique_products_basket.each do |product|
      p=Product.find(product)
      Sold.create(product_id: p.id, order_id: @order.id,
                  quantity: count_num_products_in_basket(p), price: p.price)
    end
    # vacio el carrito de la sesion
    temp_basket=session[:basket] # guardo el carrito por si la tarjeta de credito falla
    session[:basket]=Array.new
    ##################################################

    # decido donde ir segun metodo de pago
    case @order.pay_method
      when t('order.pay_method1') #----------------------------------------- transferencia
        flash[:notice] = t('order.new', ref: @order.id)
        # Mando email al cliente con informacion para realizar el pago por transferencia
        OrderNotifier.bank_transfer(@order, @web_title).deliver_now
        # Mando email al administrador para que sepa de un pedido nuevo
        OrderNotifier.admin_notify_order(@order, @web_title).deliver_now
        respond_with(@order, location: order_path(@order))
      when t('order.pay_method2') #---------------------------------------------- paypal
        redirect_to @order.paypal_url(@url) #el parametro de la funcion es el dominio la web y el token de seguridad de rails
      when t('order.pay_method3') #--------------------------------------- tarjeta de credito
        @credit_card=CreditCard.new(params[:order][:credit_card])
        if @order.check_credit_card(@credit_card) && @credit_card.valid? # compruebo que el pago sea correcto
          @order.update_attribute(:state, t('order.state2'))
          # Mando email al cliente con informacion para realizar el pago por transferencia
          OrderNotifier.credit_card(@order, @web_title).deliver_now
          # Mando email al administrador para que sepa de un pedido nuevo
          OrderNotifier.admin_notify_order(@order, @web_title).deliver_now
          respond_with(@order, location: order_path(@order))
        else # fallo el pago por tarjeta
          flash[:alert]= t('order.alert_credit_card')
          @order.destroy # elimino el pedido
          session[:basket]=temp_basket # restauro cesta
          render :new, object: @credit_card
        end
      else
        render :new
    end
  end

  private
  def order_params
    params.require(:order).permit(:address, :pay_method, :comment)
  end

end

