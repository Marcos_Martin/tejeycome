class ProductsController < ApplicationController
  def index
    @products = Product.actives.order(updated_at: :desc).paginate(page: params[:page], per_page: Product::PAGE_INDEX).order('created_at DESC')
    @web_title='TejeyCome.es - Teje'
    @description= t('blog.description_seo')
  end

  def show
    @product = Product.find(params[:id])
    @load_javascript+="run_photoswipe();"
    @web_title="TejeyCome.es - Teje (#{@product.name})"
    @url=request.original_url
    @description= "(#{@product.category.name}) #{@product.name}: "+Sanitize.fragment(@product.description).split[0...80].join(' ')
    @keywords= t('common.keywords')+", #{@product.name}"
    @image= @product.media
  end

  def category
    param=params[:category]
    @web_title="TejeyCome.es - Teje (#{param})"
    if Category.find_by(name: param).have_subcategories?
      search=(Product.where(category_id: Category.find_by(name: param).subcategories).
          union(Product.where(category_id: Category.find_by(name: param)))).where(active: true)
    else
      search= Product.where(category_id: Category.find_by(name: param).id).where(active: true)
    end
    @products = search.order(updated_at: :desc).paginate(page: params[:page], per_page: Product::PAGE_INDEX)
    @category=params[:category]
    render "index"
  end

  def search
    @web_title="TejeyCome.es - Busqueda"
    param=params[:search][:text]
    search= Product.where("name LIKE '%#{param}%'").where(active: true)
    @products = search.order(updated_at: :desc).paginate(page: params[:page], per_page: Product::PAGE_INDEX)
    @searched= params[:search][:text]
    render "index"
  end
end
