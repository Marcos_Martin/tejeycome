class ContactController < ApplicationController
  before_filter :check_humanizer_answer, :only => [:create]
  def index
    @message = Message.new
    @message.change_humanizer_question
    @web_title='TejeyCome.es - Contacto'
    @load_javascript += 'run_wysihtml5();'
  end

  def create
    @message = Message.new(params[:message])
    if @message.valid?
      ContactNotifier.contact_me(@message).deliver_now
      redirect_to(contact_index_path , notice: t('common.email_send_ok'))
    else
      flash[:alert] = t('common.email_send_wrong')
      render :index
    end
  end
end
