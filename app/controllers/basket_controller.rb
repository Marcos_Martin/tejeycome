class BasketController < ApplicationController
  #@basket contendra un array con el id de productos.
  # Los productos que se compran se van poniendo en session[:basket]
  respond_to :js

  def set_product
    @p=Product.find(params[:id])
    session[:basket] << params[:id] if @p.active
    respond_with @p
  end

  def reset_basket
    session[:basket]=Array.new
    respond_to do |format|
      format.js {render(inline: "location.reload();", notice: t('basket.reset_cart')) }
    end
  end

  def delete_basket_product
    @p=Product.find(params[:id])
    #elimina la primera coincidencia de params[:id] en session[:basket]
    session[:basket].delete_at(session[:basket].find_index(@p.id.to_s))
    respond_with @p
  end
end
