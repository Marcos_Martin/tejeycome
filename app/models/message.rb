#encoding: utf-8
class Message
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  # Configuracion de captcha humanizer
  include Humanizer
  require_human_on :all

  attr_accessor :name,:surname, :email, :subject, :body,:phone,:file,:humanizer_answer,:humanizer_question_id
  validates :name,:surname, :email, :subject, :body,presence: true
  validates :email,  format: {
                      with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                  }
  validate :validates_uploadfile

  def validates_uploadfile()
    max_size = 20971520  #bytes de 20Mbytes (correo de gmail max=25Mbytes)
    #177891bytes =173KB de windows
    if(file != nil)
      errors.add(:archivo, t('common.too_biggest_email')) if file.size > max_size
    end

  end

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

end