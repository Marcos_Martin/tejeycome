class Comment < ActiveRecord::Base
  # Configuracion de captcha humanizer
  include Humanizer
  require_human_on :create
  # Es posible crear comentarios para cualquier objeto
  validates :email, :body, :email,:user, presence: true
  validates :email,  format: {
                      with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                  }
  belongs_to :commentable, polymorphic: true

  has_many :replies, class_name: :Comment, foreign_key: :replied_of_id, dependent: :destroy

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  def slug_candidates
    [
        :user,
        [:user, :id],
    ]
  end

  PAGE_INDEX=20
end
