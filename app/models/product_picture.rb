class ProductPicture < ActiveRecord::Base
  #Galeria de imagenes para un producto
  include DirectLink
  before_save :check_direct_media_url #modulo para chekear links directos de almacenes web
  validates :media, presence: true

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  def slug_candidates
    [
        :id
    ]
  end

  belongs_to :product
end
