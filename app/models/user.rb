class User < ActiveRecord::Base
  # Configuracion de captcha humanizer
  include Humanizer
  require_human_on :create
  #Un usuario User comienza siempre como usuario cliente de la aplicacion y puede
  #ser elevado a superUsuario por el modelo AdminUser
  before_create :set_role_customer #un usuario se registra siempre como cliente

  devise :database_authenticatable, :recoverable, :rememberable,
         :trackable, :validatable, :registerable

  # se valida que el email tenga el formato correcto
  validates :email,  format: {
                      with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                      }
  # se valida que el email , nombre, primer apellido, segundo apellido, direccion y
  # telefono no sean vacios.
  validates :email, :name, :surname1, :surname2, :address, :phone, presence: true

  # listar usuarios clientes
  scope :customers, -> { where(role_id: Role.find_by(name: User::CUSTOMER_ROLE))  }
  # listar usuarios admin
  scope :admins, -> { where(role_id: Role.find_by(name: User::ADMIN_ROLE))  }
  # listar usuarios desactivados
  scope :disable, -> { where(active: false)  }
  # listar usuarios de este año
  scope :this_year, -> {where('extract(year from created_at) = ?', Time.now.year)}


  # asignacion de roles a los usuarios
  ADMIN_ROLE = "Administrador"
  CUSTOMER_ROLE = "Cliente"

  # numero de entradas para paginar usuarios
  PAGE_INDEX = 20

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  def slug_candidates
    [
        :name,
        [:name, :surname1],
        [:name, :surname1,:surname2],
    ]
  end

  belongs_to :role
  has_many :orders

  def is_admin?
    role.name == ADMIN_ROLE
  end
  def is_customer?
    role.name == CUSTOMER_ROLE
  end

######################################
  private
  def set_role_customer
    self.role_id=Role.find_by_name(CUSTOMER_ROLE).id
  end


end
