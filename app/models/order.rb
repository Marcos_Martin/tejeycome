class Order < ActiveRecord::Base
  include BasketHelper
  include PayPal::SDK::REST
  has_many :solds
  belongs_to :user

  PAGE_INDEX=10 #numero de entradas que se veran en la accion index (menu admin)

  # mostrar solo los pedidos del usuario actual en su menu de administracion
  scope :from_current_customer, -> (current_user) { where(user_id: current_user.id) }
  # mostrar los pedidos que aun no han sido enviados pero si pagados
  scope :pending_shipping, -> {where(state: I18n.t('order.state2')) }
  # mostrar los pedidos que aun no han sido pagados
  scope :outstanding_payment, -> { where(state: I18n.t('order.state1')) }
  # mostrar los pedidos realizados este mes
  scope :this_month, -> {where('created_at BETWEEN ? AND ?', Time.now.beginning_of_month,Time.now.end_of_month)}


  def new_cart

    unique_products_basket.each do |product|
      p=Product.find(product)
     Sold.create(product_id: p.id,order_id: @order.id,
                    quantity: count_num_products_in_basket(p),price: p.price)
    end
  end

  def paypal_url(url)
    # TODO repasar direcciones para produccion
    self.update_attribute(:token_paypal,SecureRandom.hex(35)) # preparo un token de vuelta para paypal
    values = {
        # Valores generales
        business: Rails.env.production??
            ENV['MAIL_USER_NAME']: ENV['PAY_PAL_VENDOR_EMAIL'], #el que recibe el pago
        cmd: '_cart',
        upload: 1,
        return: Rails.env.production??
            url.to_s+"orders/#{self.id.to_s}":'http://localhost:3000/'+"orders/#{self.id.to_s}", #pagina de respuesta, ej. /orders/1
        invoice: self.id.to_s,
        currency_code: 'EUR',
        cpp_logo_image: Rails.env.production??
            url.to_s+'assets/logoweb_paypal.png':'http://localhost:3000/assets/logoweb_paypal.png', #imagen para web checkout paypal 190x60
        lc: 'ES',
        rm: '2', # volvemos usando POST con todas las variables del pedido
        custom: self.token_paypal # este token nos sera devuelto para comprobar la autenticidad de la compra
    }
    # Valores del pedido
    values.merge!({
                      # The amount is in cents
                      "amount_1" => "%.2f" % self.price.to_s, #precio del pedido
                      "item_name_1" => "TejeyCome [Ref:#{self.id}]", #nombre del pedido
                      "item_number_1" => self.id.to_s, #referencia del pedido
                      "quantity_1" => 1 #la cantidad, que es uno siempre
                  })
    # para entorno de producccion: https://www.paypal.com/cgi-bin/webscr?
    # Informacion: https://www.paypal.com/es/cgi-bin/webscr?cmd=p/sell/ipn-test-outside

    Rails.env.production??
        "https://www.paypal.com/cgi-bin/webscr?" + values.to_query : "https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
  end
  def check_credit_card(credit_card)
    Rails.logger.error "--------chequeando tarjeta: "+ credit_card.valid?.to_s
    credit_card.valid?
    # TODO comprobar esto porque no esta probado
    # Build Payment object
    @payment = Payment.new({
                               :intent => "sale",
                               :payer => {
                                   :payment_method => "credit_card",
                                   :funding_instruments => [{
                                                                :credit_card => {
                                                                    :type => Rails.env.production?? credit_card.type : "visa",
                                                                    :number => Rails.env.production?? credit_card.number : "4567516310777851",
                                                                    :expire_month => Rails.env.production?? credit_card.expire_month : "11",
                                                                    :expire_year => Rails.env.production?? credit_card.expire_year : "2018",
                                                                    :cvv2 => Rails.env.production?? credit_card.cvv2 : "847",
                                                                    :first_name => Rails.env.production?? credit_card.first_name : "Joe",
                                                                    :last_name => Rails.env.production?? credit_card.last_name : "Shopper",
                                                                    :billing_address => {
                                                                        :line1 => self.address,
                                                                        :city => "SANTA CRUZ DE MUDELA",
                                                                        :state => "Ciudad Real",
                                                                        :postal_code => "13730",
                                                                        :country_code => "ES" }}}]},
                               :transactions => [{
                                                     :item_list => {
                                                         :items => [{
                                                                        :name => "TejeyCome [Ref:#{self.id}]", #nombre del pedido,
                                                                        :sku =>"TejeyCome [Ref:#{self.id}]", #nombre del pedido,
                                                                        :price => "%.2f" % self.price,
                                                                        :currency => "EUR",
                                                                        :quantity => 1 }]},
                                                     :amount => {
                                                         :total => "%.2f" % self.price,
                                                         :currency => "EUR" },
                                                     :description =>  "TejeyCome [Ref:#{self.id}]"}]})

    # Create Payment and return the status(true or false)
    if @payment.create
      @payment.id     # Payment Id
      true
    else
      @payment.error  # Error Hash
      false
    end
  end
end
