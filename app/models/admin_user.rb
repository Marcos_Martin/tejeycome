class AdminUser < User
  before_create :set_role_admin

  private
  def set_role_admin
    self.role_id=Role.find_by_name(ADMIN_ROLE).id
  end
end