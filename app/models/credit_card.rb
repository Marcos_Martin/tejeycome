class CreditCard
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :type,:number, :expire_month, :expire_year, :cvv2,:first_name, :last_name

  validates :type,:number, :expire_month, :expire_year, :cvv2,:first_name, :last_name, presence: true
  #validates :line1,:city,:state,:postal_code,:country_code,presence: true

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end