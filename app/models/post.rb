class Post < ActiveRecord::Base
  include DirectLink
  before_create :set_default_category
  before_save :check_direct_media_url #modulo para chekear links directos de almacenes web

  belongs_to :post_category
  has_many :comments, as: :commentable, dependent: :destroy


  scope :drafts, -> { where(draft: true).order(['created_at DESC']) } #mostrar borradores
  scope :actives, -> {where(draft: false).order(['created_at DESC'])} #mostrar activos
  scope :random4, -> { actives.order("RAND()").first(4) }
  scope :by_year, lambda { |year|  where("YEAR(created_at) =#{year}").order(['created_at DESC'])}
  scope :by_year_and_month,lambda { |year, month| where("YEAR(created_at) =#{year}").where("MONTH(created_at) = #{month}").order(['created_at DESC'])}

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  def slug_candidates
    [
        :title,
        [:title, :id],
    ]
  end

  PAGE_INDEX = 8
  def self.years
    select("created_at").order(['created_at DESC']).map{ |item| item.created_at.year }.uniq
  end

  private
  def set_default_category
    self.post_category_id=PostCategory.first.id if self.post_category_id.nil?
  end

end
