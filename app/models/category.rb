class Category < ActiveRecord::Base
  #una categoria puede tener varias subcategorias y una subcategoria pertenece a una
  #categoria. (Ver la migracion para mas detalles)

  #Category.where(parent_id: nil) lista todas las categorias

  #Category.where(parent_id: nil).last.subcategories lista todas las subcategorias
  #de la ultima categoria

  #Category.where(parent_id: nil).last.subcategories.last.parent_category
  #Lista la categoria padre de la ultima subcategoria.
  #(Siendo la categoria de donde se sacan las subcategorias, la ultima existente),


  #Category.create(name: 'Armarios') crea la categoria Armarios

  #Category.where(parent_id: nil).last.subcategories << Category.create(name: 'dos puertas')
  #A la ultima categoria le crea una subcategoria 'dos puertas'

  #Arreglo el nombre a la subcategoria agregandole [Categoria principal] - [nombre]
  before_create :adjust_name_subcategory
  before_destroy :prevent_destroy_category_varios_and_reset_product_categories

  has_many :subcategories, class_name: "Category", foreign_key: "parent_id", dependent: :destroy
  belongs_to :parent_category, class_name: "Category",foreign_key: "parent_id"
  has_many :products

  #Una categoria debe tener un nombre siempre
  validates :name, presence: true

  #Listar todas las categorias principales
  scope :mains, -> { where(parent_id: nil)  }

  #Listar todas las subcategorias
  scope :subcategories, -> { where.not(parent_id: nil)  }

  #usamos como para la ulr amistosa el nombre. Ej: /category/sofas
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  def slug_candidates
    [
        :name,
        [:name, :id],
    ]
  end

  def have_subcategories?
    self.subcategories.blank?? false : true
  end

  private
  def adjust_name_subcategory
    # A la subcategoria se le añade la categoria padre tipo Armario - Tres patas
    unless self.parent_id.blank?
      self.name=Category.find(parent_id).name+' - '+self.name
    end
  end
  def prevent_destroy_category_varios
    #no se puede eliminar la primera categoria, que sera la de varios (creada con el archivo seed.rb)
    if self.id==1
      errors.add(:base, "The school must have at least one administrator")
      # return false impide que se elimine
      return false
    end
  end
  def prevent_destroy_category_varios_and_reset_product_categories
    #Si se elimina una categoria, se buscan todos los productos con esa categoria y
    #se les pone en la categoria primera de la base la tabla categoria de la base de datos
    #La primera categoria siempre sera varios pues es la primera que se creo al llamar
    #a rake:db:seed al instalar la aplicacacion

    #no se puede eliminar la primera categoria, que sera la de varios (creada con el archivo seed.rb)
    if self.id==Category.first.id
      errors.add(:base, I18n.t('admin_zone.categories.prevent_destroy_category'))
      # return false impide que se elimine
      return false
    else
      Product.where(category_id: self.id).update_all(category_id: Category.first.id)
    end
  end
end
