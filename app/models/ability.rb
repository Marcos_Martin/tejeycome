class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.is_admin?
      can :manage, :all
    elsif user.is_customer?
      can :read, Order, user_id: user.id
      can [:index], :home
      can [:search], Order
    end
  end
end