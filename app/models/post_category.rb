class PostCategory < ActiveRecord::Base
  before_destroy :prevent_destroy_category_varios_and_reset_post_categories
  has_many :posts

  #Una categoria debe tener un nombre siempre
  validates :name, presence: true

  #usamos como para la ulr amistosa el nombre. Ej: /category/sofas
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  def slug_candidates
    [
        :name,
        [:name, :id],
    ]
  end

  private
  def prevent_destroy_category_varios_and_reset_post_categories
    #Si se elimina una categoria, se buscan todos los productos con esa categoria y
    #se les pone en la categoria primera de la base la tabla categoria de la base de datos
    #La primera categoria siempre sera varios pues es la primera que se creo al llamar
    #a rake:db:seed al instalar la aplicacacion

    #no se puede eliminar la primera categoria, que sera la de varios (creada con el archivo seed.rb)
    if self.id==PostCategory.first.id
      errors.add(:base, I18n.t('admin_zone.categories.prevent_destroy_category'))
      # return false impide que se elimine
      return false
    else
      Post.where(post_category_id: self.id).update_all(post_category_id: PostCategory.first.id)
    end
  end

end
