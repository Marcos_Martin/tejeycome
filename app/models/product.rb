class Product < ActiveRecord::Base
  include DirectLink
  #si el producto viene sin categoria se le asigna la primera existente en Category, que
  #segun el archivo seed.rb sera Varios
  before_create :set_default_category
  before_save :check_direct_media_url #modulo para chekear links directos de almacenes web

  #No se podra guardar un producto si no existe un nombre, descripcion y precio
  validates :name,:description,:price,:media, presence: true

  # listar los productos activos
  scope :actives, -> { where(active: true)  }

  # listar los productos desactivados
  scope :deactivates, -> { where(active: false)  }

  # listar 6 produztos al azar
  scope :random4, -> { actives.order("RAND()").first(4) }

  PAGE_INDEX=12 #numero de entradas que se veran en la accion index (multiplo de 4)
  #usamos como para la ulr amistosa el nombre. Ej: /product/sofa-blanco

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  def slug_candidates
    [
        :name,
        [:name, :id],
    ]
  end

  has_many :product_pictures, dependent: :destroy
  belongs_to :category
  has_many :solds

  private
  def set_default_category
    self.category_id=Category.where(parent_id: nil).first.id if category_id.nil?
  end
end
