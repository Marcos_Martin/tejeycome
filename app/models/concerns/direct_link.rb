module DirectLink
  extend ActiveSupport::Concern
  def get_google_direct_link(url)
    direct_link="/assets/missing_photo.png"
    if url.include?("https://drive.google.com/uc?export=") #si esta transformada no se cambia
      direct_link=url
    elsif url.include?("https://drive.google.com/file/d/") # si viene de esta manera, se transforma
      temp=url.split("https://drive.google.com/file/d/").last.split("/view?usp=sharing").first
      direct_link="https://drive.google.com/uc?export=download&id="+temp
    elsif url.include?("https://drive.google.com/open?id=")# o tambien puede venir de esta
      temp=url.split("https://drive.google.com/open?id=").last
      direct_link="https://drive.google.com/uc?export=download&id="+temp
    end
    direct_link
  end
  def url_is_google_drive?(url)
    url.include?("https://drive.google.com/")? true : false
  end
  def check_direct_media_url
    if url_is_google_drive?(self.media) # si la url es de google drive se transforma a link directo
      self.media=get_google_direct_link(self.media)
    end
  end
end