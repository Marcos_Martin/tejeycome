class ContactNotifier < ApplicationMailer
  def contact_me(message)
    @message=message
    file=message.file
    unless file.blank?
      attachments[file.original_filename] = File.open(file.path, 'rb'){|f| f.read}
    end
    #TODO esto hay que cambiarlo para poner el email de produccion segun /enviroments/production.rb
    mail to:  "TejeyCome <tejeycome@gmail.com>",
         subject: "[TejeyCome.es - Contacto]: "+message.subject,
         from: "TejeyCome <#{ENV['MAIL_USER_NAME']}>"
  end
end
