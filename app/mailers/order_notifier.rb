class OrderNotifier < ApplicationMailer
  # TODO comprobar los from: de los emails para produccion
  def bank_transfer(order,web_title)
    @order=order
    mail to: @order.user.email,
         subject: "[#{web_title.to_s}]: "+t('order.email_sale_subject',ref: @order.id ),
         from: "TejeyCome <#{ENV['MAIL_USER_NAME']}>"
  end
  def paypal(order,web_title)
    @order=order
    mail to: @order.user.email,
         subject: "[#{web_title.to_s}]: "+t('order.email_sale_subject',ref: @order.id ),
         from: "TejeyCome <#{ENV['MAIL_USER_NAME']}>"
  end
  def credit_card(order,web_title)
    @order=order
    mail to: @order.user.email,
         subject: "[#{web_title.to_s}]: "+t('order.email_sale_subject',ref: @order.id ),
         from: "TejeyCome <#{ENV['MAIL_USER_NAME']}>"
  end
  def admin_notify_order(order,web_title)
    @order=order
    mail to: "TejeyCome <tejeycome@gmail.com>",
         subject: "[#{web_title.to_s}] #{t('order.email_new_sale')}: "+t('order.email_sale_subject',
                                                                         ref: @order.id ),
         from: "TejeyCome <#{ENV['MAIL_USER_NAME']}>"
  end
  def change_order(order,web_title)
    @order=order
    mail to: @order.user.email,
         subject: "[#{web_title.to_s}]: "+t('order.email_change_order',ref: @order.id ),
         from: "TejeyCome <#{ENV['MAIL_USER_NAME']}>"
  end
end
