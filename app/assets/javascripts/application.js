//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap.min
//= require photoswipe.js
//= require admin-lte
//= require ckeditor/init
//= require bootstrap-wysihtml5
//= require bootstrap-wysihtml5/locales/es-ES
//= require jsapi
//= require chartkick
//= require cookies_eu
//= require bootstrap-submenu.min
//= require cookies_eu
//= require turbolinks
//= require_tree .

// evita algunos problemas con los turbolinks
// turbolinks estan desactivados para la zona de administracion
$(document).ready(function () {
    $('.dropdown-toggle').dropdown();
    $.AdminLTE.layout.activate();
    $('#carousel-example-generic').carousel({interval: 3000});
    run_photoswipe();

    $('[data-submenu]').submenupicker();
    run_equalizer_columns();
});
$(document).on('page:load', function () {

    var o;
    o = $.AdminLTE.options;
    if (o.sidebarPushMenu) {
        $.AdminLTE.pushMenu.activate(o.sidebarToggleSelector);
    }
    $.AdminLTE.layout.activate();
    window['rangy'].initialized = false
});
// evita algunos problemas con los turbolinks

function run_wysihtml5() {
    $(document).ready(function () {
        $('.wysihtml5').wysihtml5({
            locale: "es-ES", 'toolbar': {
                'font-styles': false,
                'color': true,
                'emphasis': {
                    'small': true
                },
                'blockquote': true,
                'lists': true,
                'html': false,
                'link': true,
                'image': true,
                'smallmodals': true
            }
        });
    })
}

function run_equalizer_columns() {
    $(document).ready(function () {
        var heights = $(".equalize").map(function () {
                return $(this).height();
            }).get(),

            maxHeight = Math.max.apply(null, heights);

        $(".equalize").height(maxHeight);
    });
}
