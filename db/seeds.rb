#Creo los roles
Role.create(name: 'Administrador')
Role.create(name: 'Cliente')
#Creo el primer administrador
AdminUser.create(email: 'tejeycome@gmail.com',
                         password: 'tejecome1981',
                         password_confirmation: 'tejecome1981',
                         name: "Admin",
                         surname1: "dulco",
                         surname2: "dulco",
                         address: "Address",
                         phone: "627 98 08 77"
)
#Creo el primer usuario, que podria ser uno de pruebas
User.create(email: 'marcos.martingm@gmail.com',
                 password: 'tejecome1981',
                 password_confirmation: 'tejecome1981',
                 name: "Marcos",
                 surname1: "Martin",
                 surname2: "Gª-Muñoz",
                 address: "Ciudad Real",
                 phone: "667095386"
)
#Creo la primera categoria base, un producto partira siempre con esta categoria por defecto
Category.create(name: "Varios")
PostCategory.create(name: "Varios")

DeliveryPrice.create(gr: -1, price: 8.47) # gastos de envio para contrareembolso en casa
DeliveryPrice.create(gr: 0, price: 0) # sin gastos de envio
DeliveryPrice.create(gr: 0.01, price: 0)
DeliveryPrice.create(gr: 20, price: 2.84)
DeliveryPrice.create(gr: 50, price: 2.97)
DeliveryPrice.create(gr: 100, price: 3.34)
DeliveryPrice.create(gr: 500, price: 4.45)
DeliveryPrice.create(gr: 1000, price: 7.00)
DeliveryPrice.create(gr: 2000, price: 7.61)