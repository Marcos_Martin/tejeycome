class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.text :address
      t.string :pay_method
      t.text :comment
      t.string :state, default: 'Pendiente de pago'
      t.string :cod_delivery #codigo de envio de correos
      t.decimal :delivery_price, precision: 8, scale: 2, default:0
      t.decimal :price, precision: 8, scale: 2
      t.string :token_paypal # token para que paypal en la vuelta tenga seguridad

      t.timestamps null: false
    end
  end
end
