class CreateDeliveryPrices < ActiveRecord::Migration
  def change
    create_table :delivery_prices do |t|
      t.decimal :gr, precision: 8, scale: 2,unique: true
      t.decimal :price, precision: 8, scale: 2
    end
  end
end
