class CreateSolds < ActiveRecord::Migration
  def change
    create_table :solds do |t|
      t.integer :product_id
      t.integer :order_id
      t.decimal :price, precision: 8, scale: 2
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
