class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.decimal :price, precision: 8, scale: 2
      t.boolean :active, default: true #este atributo hara que productos desaparezcan
                                                 # de la tienda pero no de la base de datos
      t.decimal :gr, precision: 8, scale: 2, default: 2
      t.string :media,  default: "/assets/missing_photo.png" #esta sera la imagen principal
      t.integer :category_id
      t.string :slug

      t.timestamps null: false
    end
    add_index :products, :slug, unique: true
  end
end
