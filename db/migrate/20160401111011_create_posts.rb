class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :body
      t.string :title
      t.boolean :draft, default: false # crearlo como borrador=falso
      t.integer :post_category_id
      t.string :slug
      t.string :media, default: "/assets/missing_photo.png" #esta sera la imagen principal
      t.timestamps
    end
    add_index :posts, :slug, unique: true
  end
end
