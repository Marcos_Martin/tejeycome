class CreateProductPictures < ActiveRecord::Migration
  def change
    create_table :product_pictures do |t|
      t.integer :product_id
      t.string :media, default: "/assets/missing_photo.png" #estas seran las imagenes secundarias
      t.string :slug
    end
    add_index :product_pictures, :slug, unique: true
  end
end
