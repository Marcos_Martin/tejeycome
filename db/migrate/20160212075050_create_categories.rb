class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string      :name
      t.references  :parent
      t.string :slug

    end
    add_index :categories, :slug, unique: true
  end
end
