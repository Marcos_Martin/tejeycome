# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160419082622) do

  create_table "categories", force: :cascade do |t|
    t.string  "name",      limit: 255
    t.integer "parent_id", limit: 4
    t.string  "slug",      limit: 255
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true, using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "body",             limit: 65535
    t.string   "user",             limit: 255
    t.string   "email",            limit: 255
    t.integer  "replied_of_id",    limit: 4
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.string   "slug",             limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delivery_prices", force: :cascade do |t|
    t.decimal "gr",    precision: 8, scale: 2
    t.decimal "price", precision: 8, scale: 2
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.text     "address",        limit: 65535
    t.string   "pay_method",     limit: 255
    t.text     "comment",        limit: 65535
    t.string   "state",          limit: 255,                           default: "Pendiente de pago"
    t.string   "cod_delivery",   limit: 255
    t.decimal  "delivery_price",               precision: 8, scale: 2, default: 0.0
    t.decimal  "price",                        precision: 8, scale: 2
    t.string   "token_paypal",   limit: 255
    t.datetime "created_at",                                                                         null: false
    t.datetime "updated_at",                                                                         null: false
  end

  create_table "post_categories", force: :cascade do |t|
    t.string "name", limit: 255
    t.string "slug", limit: 255
  end

  add_index "post_categories", ["slug"], name: "index_post_categories_on_slug", unique: true, using: :btree

  create_table "posts", force: :cascade do |t|
    t.text     "body",             limit: 65535
    t.string   "title",            limit: 255
    t.boolean  "draft",                          default: false
    t.integer  "post_category_id", limit: 4
    t.string   "slug",             limit: 255
    t.string   "media",            limit: 255,   default: "/assets/missing_photo.png"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "posts", ["slug"], name: "index_posts_on_slug", unique: true, using: :btree

  create_table "product_pictures", force: :cascade do |t|
    t.integer "product_id", limit: 4
    t.string  "media",      limit: 255, default: "/assets/missing_photo.png"
    t.string  "slug",       limit: 255
  end

  add_index "product_pictures", ["slug"], name: "index_product_pictures_on_slug", unique: true, using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.decimal  "price",                     precision: 8, scale: 2
    t.boolean  "active",                                            default: true
    t.decimal  "gr",                        precision: 8, scale: 2, default: 2.0
    t.string   "media",       limit: 255,                           default: "/assets/missing_photo.png"
    t.integer  "category_id", limit: 4
    t.string   "slug",        limit: 255
    t.datetime "created_at",                                                                              null: false
    t.datetime "updated_at",                                                                              null: false
  end

  add_index "products", ["slug"], name: "index_products_on_slug", unique: true, using: :btree

  create_table "roles", force: :cascade do |t|
    t.string "name", limit: 255
  end

  create_table "solds", force: :cascade do |t|
    t.integer  "product_id", limit: 4
    t.integer  "order_id",   limit: 4
    t.decimal  "price",                precision: 8, scale: 2
    t.integer  "quantity",   limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",   null: false
    t.string   "encrypted_password",     limit: 255, default: "",   null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "name",                   limit: 255
    t.string   "surname1",               limit: 255
    t.string   "surname2",               limit: 255
    t.string   "address",                limit: 255
    t.string   "phone",                  limit: 255
    t.boolean  "active",                             default: true
    t.integer  "role_id",                limit: 4
    t.string   "slug",                   limit: 255
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

end
