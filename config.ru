# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment', __FILE__)
# controlar peticiones abusivas a la web
use Rack::Attack
run Rails.application
