namespace :database do
  desc "TODO"
  task :reset => :environment do
    File.delete("#{Rails.root}/db/schema.rb")
    Rake::Task["db:migrate:reset"].invoke
    p "--------------RESET OK"
    Rake::Task["db:seed"].invoke
    p "--------------SEED OK"
  end
  task :seed_fakes => :environment do
    for i in 0..30
      rand_time= Time.now-rand(365).day
      User.create(email: Faker::Internet.email,
                  password: 'dulco1981',
                  password_confirmation: 'dulco1981',
                  name: Faker::Name.name,
                  surname1: Faker::Name.first_name,
                  surname2: Faker::Name.last_name,
                  address: Faker::Address.street_address,
                  phone: Faker::PhoneNumber.phone_number,
                  created_at: rand_time,
                  updated_at: rand_time
      )
    end
    p "--------------creados 30 usuarios OK"
################################################
    Category.create(name: "Ganchillo")
    Category.create(name: "Con cuerda", parent_id: Category.find_by(name: "Ganchillo").id)
    Category.create(name: "Con cadena", parent_id: Category.find_by(name: "Ganchillo").id)
    Category.create(name: "Bisutería")
    Category.create(name: "Pendientes", parent_id: Category.find_by(name: "Bisutería").id)
    Category.create(name: "Cocina")

    p "--------------creadas categorias de prueba OK"
################################################
    p "--------------generando 30 productos con 4 imagenes cada uno ..."
    for i in 0..60
      product=Product.create(name: Faker::Commerce.product_name,
                             description: Faker::Hacker.say_something_smart,
                             price: Faker::Commerce.price,
                             category_id: Category.order("RAND()").first.id,
                             active: rand(0..1),
                             media: Faker::Avatar.image
      )
      for i in 0..4
        ProductPicture.create(
            product_id: product.id,
            media: Faker::Avatar.image
        )
      end
    end
    p "--------------creados 60 productos OK"
################################################
# Metemos 30 pedidos con un maximo de 4 elementos por pedido y a fecha
# entre hoy y 40 dias atras
    p "--------------generando 30 pedidos ..."
    pay_methods=["Transferencia bancaria", "PayPal", "Tarjeta de crédito"]
    states=["Pendiente de pago", "Pagado. En proceso de creación", "Enviado"]

    for i in 0..30
      rand_time= Time.now-rand(40).day
      total_price=0
      user_order= User.find_by(id: 2)
      if i>5
        user_order=User.order("RAND()").first # meto 5 pedidos en el usuario de pruebas
      end
      order=Order.create(user_id: user_order.id,
                         pay_method: pay_methods.sample,
                         state: states.sample, created_at: rand_time,
                         address: user_order.address,
                         updated_at: rand_time)
      for i in 0..4
        sold_product= Product.order("RAND()").first
        sold=Sold.create(product_id: sold_product.id,
                         order_id: Order.last.id,
                         price: sold_product.price,
                         quantity: rand(1..5),
                         created_at: rand_time,
                         updated_at: rand_time)
        total_price+=sold.price*sold.quantity
      end
      order.update_attributes(price: total_price)
    end
    p "--------------creados 30 pedidos OK"
################################################
    p "--------------generando 30 entradas de blog con 10 comentarios por entrada ..."
    for i in 0..30
      rand_time1= Time.now-rand(365).day
      post=Post.create(body: Faker::Lorem.paragraph(10, false, 4),
                       title: Faker::Book.title,
                       draft: rand(0..1),
                       media: Faker::Avatar.image,
                       created_at: rand_time1,
                       updated_at: rand_time1)
      for i in 0..10
        rand_time2= Time.now-rand(365).day
        Comment.create(body: Faker::Lorem.paragraph(2, false, 1),
                       user: Faker::Internet.user_name,
                       email: Faker::Internet.email,
                       commentable_id: post.id,
                       commentable_type: Post::name,
                       created_at: rand_time2,
                       updated_at: rand_time2)
      end
    end
    p "--------------creados 30 entradas de blog  OK"
  end
end