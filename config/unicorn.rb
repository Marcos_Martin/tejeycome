# app_name/config/unicorn.rb

worker_processes 2

APP_PATH = "/home/alfred/webs/tejeycome"
working_directory APP_PATH # available in 0.94.0+

listen "/home/alfred/webs/tejeycome/tmp/sockets/unicorn.app2.sock", :backlog => 64
listen 8081, :tcp_nopush => true

timeout 30

pid APP_PATH + "/tmp/pids/unicorn2.pid"

stderr_path APP_PATH + "/log/unicorn2.stderr.log"
stdout_path APP_PATH + "/log/unicorn2.stdout.log"
