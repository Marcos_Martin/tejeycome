class Rack::Attack
  @ip='hacker'
  # Acelerar grandes volúmenes de solicitudes por dirección IP
  throttle('req/ip', limit: 20, period: 20.seconds) do |req|
    @ip=req.ip
    req.ip unless req.path.starts_with?('/assets')
  end

  # Acelera intentos de inicio de sesión por dirección IP
  throttle('logins/ip', limit: 5, period: 20.seconds) do |req|
    @ip=req.ip
    if req.path == '/auth/login' && req.post?
      req.ip
    end
  end

  # Acelera intentos de inicio de sesión de por dirección de correo electrónico
  throttle("logins/email", limit: 5, period: 20.seconds) do |req|
    if req.path == '/auth/login' && req.post?
      req.params['email'].presence
    end
  end

  # Acelera intentos de comentarios
  throttle("logins/email", limit: 2, period: 20.seconds) do |req|
    if (req.path.include? '/comments') && req.post?
      req.params['comment'].presence
    end
  end

  # Respuesta al exceso de peticiones
  self.throttled_response = lambda do |env|
    [ 429, {}, ["Uff dejarme respirar!... demasiadas peticiones amigo #{@ip}"]]
  end
end