Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  root "home#index"
  get 'legal' => 'home#legal', as: 'legal'
  resources :how_do, only: [:index]
  resources :blog, only: [:index, :show] do
    resources :comments, only: [:create] do
    end
    collection do
      get 'category/:category/' => 'blog#category', as: 'category'
      get 'do_pdf/:post/' => 'blog#do_pdf', as: 'do_pdf'
    end
  end

  devise_for :users,
             controllers: {registrations: 'users/registrations'},
             path: "auth",
             path_names:
                 {sign_in: 'login', sign_out: 'logout', password: 'secret',
                  confirmation: 'verification', unlock: 'unblock',
                  registration: 'register', sign_up: 'cmon_let_me_in'
                 }
  resources :products, only: [:index, :show] do
    collection do
      get 'category/:category/' => 'products#category', as: 'category'
      get 'search' => 'products#search', as: 'search'
    end
  end

  post "basket/set_product" => 'basket#set_product', as: 'set_product'
  delete 'reset_basket' => 'basket#reset_basket'
  delete 'delete_basket_product/:id(.:format)', as: "delete_basket_product",
         to: 'basket#delete_basket_product'

  resources :orders, only: [:show, :new, :create]
  post "orders/:id(.:format)" => 'orders#show'
  resources :contact, only: [:index, :create]

  ######################### ADMIN ZONE ###############
  namespace :admin do
    get 'home' => 'home#index'
    post 'check_to_direct_link' => 'home#check_to_direct_link', as: 'check_to_direct_link'

    resources :users, except: [:show, :new, :create] do
      collection do
        get 'search' => 'users#search', as: 'search'
        get 'sort/:by' => 'users#sort', as: 'sort'
      end
    end

    #un producto puede contener imagenes de ProductPicture. Ademas se puede
    #buscar un producto desde el controlador producto en admin/product/search
    resources :products, except: [:show] do
      resources :product_pictures, only: [:create, :destroy]
      collection do
        get 'search' => 'products#search', as: 'search'
        get 'sort/:by' => 'products#sort', as: 'sort'
        get 'category/:category/' => 'products#category', as: 'category'
      end
    end

    resources :categories, except: [:show, :new, :edit]
    resources :post_categories, except: [:show, :new, :edit]

    resources :orders, only: [:index, :destroy, :update] do
      collection do
        get 'search' => 'orders#search', as: 'search'
        get 'sort/:by' => 'orders#sort', as: 'sort'
      end
    end

    resources :delivery_prices, except: [:show]

    resources :posts do
      collection do
        get 'search' => 'posts#search', as: 'search'
        get 'sort/:by' => 'posts#sort', as: 'sort'
        get 'category/:category/' => 'posts#category', as: 'category'
      end
    end

    resources :comments, except: [:show, :new, :create] do
      collection do
        get 'search' => 'comments#search', as: 'search'
      end
    end
  end
end
